<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "review";
    protected $fillable = [
        'nilai',
        'komentar',
        'id_lapangan',
        'user_id'
    ];
}

