<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Member;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->where('status', 2)
                    ->orWhere('status', 3)
                    ->get();
        return view('member.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profil = DB::table('profil')->where('user_id',$id)->first();
        return view('member.show', compact('profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = DB::table('users')->where('id',$id)->first();
        return view('member.edit', compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $affected = DB::table('users')
                    ->where('id', $id)
                    ->update([
                        "username"  => $request["username"],
                        "email"     => $request["email"],                        
                        "status"    => 3
                    ]);
            
        return redirect('member');
    }
    public function update2(Request $request, $id)
    {
        $affected = DB::table('users')
                    ->where('id', $id)
                    ->update([
                        "username"  => $request["username"],
                        "email"     => $request["email"],                        
                        "status"    => 3
                    ]);
          
        $query = DB::table('profil')->insert([
            "user_id"  => $request["id"],
            "alamat"     => " ",
            "bio"  => " ",
            "nama" => " ",
            "status"    => 3
        ]);
        return redirect('member');
    }
    public function updateprofil(Request $request, $id)
    {
        $affected = DB::table('profil')
                    ->where('user_id', $id)
                    ->update([
                        "nama"    => $request["nama"],
                        "alamat"  => $request["alamat"],
                        "bio"     => $request["bio"],
                        "status"  => 3
                    ]);
            
        return redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = DB::table('profil')->where('user_id',$id)->delete();
        $users = DB::table('users')->where('id',$id)->delete();        
        return redirect('member');
    }
}
