<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pegawai;
use Illuminate\Support\Facades\Hash;
class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $users = DB::table('users')->where('status', 0)
                    ->orWhere('status', 1)
                    ->get();
        return view('pegawai.index', compact('users'));
    }

    public function profil()
    {
        $users = DB::table('profil')
                    ->join('users', 'profil.user_id', '=', 'users.id')            
                    ->where('profil.status', 3)                    
                    ->get();
        return view('pegawai.profilmember', compact('users'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pegawai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       
        $query = DB::table('users')->insert([
            "username"  => $request["username"],
            "email"     => $request["email"],
            "password" => Hash::make($request['password']),
            "status"    => 0
        ]);
        return redirect('pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $profil = DB::table('profil')->where('user_id',$id)->first();
        return view('pegawai.show', compact('profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = DB::table('users')->where('id',$id)->first();
        return view('pegawai.edit', compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $affected = DB::table('users')
                    ->where('id', $id)
                    ->update([
                        "username"  => $request["username"],
                        "email"     => $request["email"],                        
                        "status"    => 1
                    ]);
            
        return redirect('pegawai');
    }
    public function update2(Request $request, $id)
    {
        $affected = DB::table('users')
                    ->where('id', $id)
                    ->update([
                        "username"  => $request["username"],
                        "email"     => $request["email"],                        
                        "status"    => 1
                    ]);
          
        $query = DB::table('profil')->insert([
            "user_id"  => $request["id"],
            "alamat"     => " ",
            "bio"  => " ",
            "nama" => " ",
            "status"    => 1
        ]);
        return redirect('pegawai');
    }
       public function updateprofil(Request $request, $id)
    {
        $affected = DB::table('profil')
                    ->where('user_id', $id)
                    ->update([
                        "nama"    => $request["nama"],
                        "alamat"  => $request["alamat"],
                        "bio"     => $request["bio"],
                        "status"  => 1
                    ]);
            
        return redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pegawai = DB::table('profil')->where('user_id',$id)->delete();
        $users = DB::table('users')->where('id',$id)->delete();        
        return redirect('pegawai');
    }
}
