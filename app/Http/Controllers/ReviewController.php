<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Review;
use App\Lapangan;
use App\User;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $order = DB::table('order')
                    ->leftJoin('review', 'order.id_lapangan', '=', 'review.id_lapangan','order.user_id','=','review.user_id')
                    ->join('lapangan','order.id_lapangan','=','lapangan.lapangan_id')
                    ->join('users','order.user_id','=','users.id')
                    ->get();
        return view('review.index', compact('order'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function review_member()
    {

        $member = DB::table('order')
                    ->leftJoin('review', 'order.id_lapangan', '=', 'review.id_lapangan','order.user_id','=','review.user_id')
                    ->join('lapangan','order.id_lapangan','=','lapangan.lapangan_id')
                    ->join('users','order.user_id','=','users.id')
                    ->get();
        return view('review.review_member', compact('member'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $lapangan = $request->input('lapangan');
        $user=$request->input('user');
        // $users = User::pluck('user','user.id');
        // return view('review.create', compact('lapangan', 'users'));
        return view('review.create',compact('lapangan','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = DB::table('review')->insert([
            "nilai" => $request["nilai"],
            "komentar" => $request["komentar"],
            "id_lapangan" => $request["lapangan_id"],
            "user_id" => $request["user_id"],
        ]);

        return redirect('review_member');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $review = DB::table('review')
        ->join('lapangan', 'lapangan.lapangan_id', '=', 'review.id_lapangan')                   
        ->join('users', 'users.id', '=', 'review.user_id')
        ->where('review.review_id',$id)
        ->first();
        
    
        return view('review.edit', compact('review'));
    }

   
    
    public function update(Request $request, $id)
    {
        $affected = DB::table('review')
                    ->where('review_id', $id)
                    ->update([
                        "nilai" => $request["nilai"],
                        "komentar" => $request["komentar"],
                    ]);

            return redirect('review_member');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $review = DB::table('review')->where('review_id',$id)->delete();
        return redirect('review');
    }
}
