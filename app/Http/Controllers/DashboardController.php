<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $tipe = DB::table('tipe')
                     ->select(DB::raw('count(*) as jumlah'))                     
                     ->get();  
        $pegawai = DB::table('profil')
                     ->select(DB::raw('count(*) as jumlah'))
                     ->where('status', '=', 1)                     
                     ->get(); 
        $member = DB::table('profil')
                     ->select(DB::raw('count(*) as jumlah'))
                     ->where('status', '=', 3)                     
                     ->get(); 
        $order = DB::table('order')
                     ->select(DB::raw('count(*) as jumlah'))                     
                     ->get();
        $review = DB::table('review')
                     ->select(DB::raw('count(*) as jumlah'))                     
                     ->get();
        $lapangan = DB::table('lapangan')
                     ->select(DB::raw('count(*) as jumlah'))                     
                     ->get();        
        return view('dashboard', compact('tipe','pegawai','member','order','review','lapangan'));
    }
}
