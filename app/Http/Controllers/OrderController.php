<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Lapangan;
use App\Tipe;
use App\Order;
use App\Waktu;

class OrderController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // $lapangan = Lapangan::all();
        $order = DB::table('order')
                    ->join('lapangan', 'lapangan.lapangan_id', '=', 'order.id_lapangan')
                    ->join('waktu', 'waktu.waktu_id', '=', 'order.id_waktu')                    
                    ->join('users', 'users.id', '=', 'order.user_id')
                    ->get();
        return view('orders.index', compact('order'));
    }
    
    public function reservasi()
    {
        // $lapangan = Lapangan::all();
        $order = DB::table('order')
                    ->join('lapangan', 'lapangan.lapangan_id', '=', 'order.id_lapangan')
                    ->join('waktu', 'waktu.waktu_id', '=', 'order.id_waktu')                    
                    ->join('users', 'users.id', '=', 'order.user_id')
                    ->get();
        return view('orders.reservasi', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $lapangan = Lapangan::pluck('nama_lapangan', 'lapangan_id');
        $waktu = Waktu::pluck('waktu_nama', 'waktu_id');
        return view('orders.create', compact('lapangan','waktu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['id_waktu'] = $request->input('id_waktu');
        Order::create($input);
        $lapangan = DB::table('lapangan')
                    ->join('tipe', 'tipe.tipe_id', '=', 'lapangan.id_tipe')
                    ->get();
        return redirect()->route('order.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($order_id)
    {
        
        $order = DB::table('order')
        ->join('lapangan', 'lapangan.lapangan_id', '=', 'order.id_lapangan')
        ->join('waktu', 'waktu.waktu_id', '=', 'order.id_waktu')                    
        ->join('users', 'users.id', '=', 'order.user_id')
        ->where('order.order_id',$order_id)
        ->first();
        
        $lapangan = Lapangan::pluck('nama_lapangan', 'lapangan_id');
        $waktu = Waktu::pluck('waktu_nama', 'waktu_id');
        return view('orders.edit', compact('order','lapangan','waktu'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $order_id)
    {
        $affected = DB::table('order')
                    ->where('order_id', $order_id)
                    ->update([
                        "order_tgl" => $request["order_tgl"],
                        "id_waktu" => $request["id_waktu"],
                        "id_lapangan" => $request["id_lapangan"]
                    ]);

            return redirect('order');

    }
    
      public function konfirmasi(Request $request, $order_id)
    {
        $affected = DB::table('order')
                    ->where('order_id', $order_id)
                    ->update([
                        "order_status" => 1
                    ]);

            return redirect('order');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id)
    {
        $lapangan = DB::table('order')->where('order_id',$order_id)->delete();
        return redirect('order');

    }
}
