<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Lapangan;
use App\Tipe;

class LapanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        // $lapangan = Lapangan::all();
        $lapangan = DB::table('lapangan')
                    ->join('tipe', 'tipe.tipe_id', '=', 'lapangan.id_tipe')
                    ->get();
        return view('lapangan.index', compact('lapangan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $tipe = Tipe::pluck('nama', 'tipe_id');
        return view('lapangan.create', compact('tipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = DB::table('lapangan')->insert([
            "nama_lapangan" => $request["nama"],
            "deskripsi" => $request["deskripsi"],
            "harga_lapangan" => $request["harga"],
            "id_tipe" => $request["tipe"],
            "status_lapangan" => "Available"
        ]);

        return redirect('lapangan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lapangan_id)
    {
        
        $tipe = Tipe::pluck('nama', 'tipe_id');
        $lapangan = DB::table('lapangan')
                    ->join('tipe', 'tipe.tipe_id', '=', 'lapangan.id_tipe')
                    ->where('lapangan.lapangan_id',$lapangan_id)
                    ->first();
        return view('lapangan.edit', compact('lapangan', 'tipe'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lapangan_id)
    {
        $affected = DB::table('lapangan')
                    ->where('lapangan_id', $lapangan_id)
                    ->update([
                        "nama_lapangan" => $request["nama"],
                        "deskripsi" => $request["deskripsi"],
                        "harga_lapangan" => $request["harga"],
                        "id_tipe" => $request["tipe"]
                    ]);

            return redirect('lapangan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lapangan_id)
    {
        $lapangan = DB::table('lapangan')->where('lapangan_id',$lapangan_id)->delete();
        return redirect('lapangan');

    }
}
