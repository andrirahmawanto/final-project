<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Tipe;
class TipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $tipe = Tipe::all();
        return view('tipe.index', compact('tipe'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipe.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = DB::table('tipe')->insert([
            "nama" => $request["nama"]
        ]);

        return redirect('tipe');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tipe_id)
    {
        // $param = \Request::segment(2);
        // dd($param);
        $tipe = DB::table('tipe')->where('tipe_id',$tipe_id)->first();
        return view('tipe.edit', compact('tipe'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tipe_id)
    {
        $affected = DB::table('tipe')
                    ->where('tipe_id', $tipe_id)
                    ->update([
                    "nama" => $request["nama"]
                    ]);

            return redirect('tipe');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tipe_id)
    {
        $tipe = DB::table('tipe')->where('tipe_id',$tipe_id)->delete();
        return redirect('tipe');

    }
}
