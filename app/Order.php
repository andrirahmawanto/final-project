<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "order";
    protected $fillable = [
        'order_tgl',
        'order_status',
        'id_waktu',
        'id_lapangan',
        'user_id'
    ];
}
