<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->bigIncrements('order_id');
            $table->date('order_tgl');
            $table->bigInteger('order_status');
            $table->unsignedBigInteger('id_waktu');
            $table->foreign('id_waktu')->references('waktu_id')->on('waktu');
            $table->unsignedBigInteger('id_lapangan');
            $table->foreign('id_lapangan')->references('lapangan_id')->on('lapangan');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
