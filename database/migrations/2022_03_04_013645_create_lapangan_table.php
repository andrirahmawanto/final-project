<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLapanganTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lapangan', function (Blueprint $table) {
            $table->bigIncrements('lapangan_id');
            $table->string('nama_lapangan');
            $table->text('deskripsi');
            $table->string('harga_lapangan');
            $table->string('status_lapangan');
            $table->unsignedBigInteger('id_tipe');
            $table->foreign('id_tipe')->references('tipe_id')->on('tipe');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lapangan');
    }
}
