@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Tipe Lapangan</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Tipe Lapangan
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            <a class="btn btn-primary" href="/tipe/create" >Tambah Tipe Lapngan</a>           
                        <div class="card-body">
                            <br>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 10%">Nama Tipe</th>
                                    <th style="text-align: center; width: 10%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($tipe as $key => $tipe)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $tipe->nama }}</td>                                
                                <td style="display: flex">                                                                        
                                    <form action="/tipe/{{ $tipe->tipe_id }}" method="POST">
                                    <a href="/tipe/{{ $tipe->tipe_id }}/edit" class="btn btn-warning btn-sm" > Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm" >
                                    </form>                                   
                                </td>
                            </tr>
                            @endforeach                     
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    
<script>
    $(document).ready(function() {    
      swal("Selamat Datang di Halaman Admin", "", "success");
    });
    </script>
@endpush
