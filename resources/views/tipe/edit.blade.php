@extends('layouts.master')
@section('content')
 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Tipe Lapangan</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Tipe Lapangan
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                    <form action="/tipe/{{ $tipe->tipe_id }}" method="POST">
                        @csrf 
                        @method('PUT')
                        <div class="card-body">
                        <div class="form-group">
                        <label for="exampleInputNama">Nama</label>
                        <input type="text" class="form-control" id="exampleInputNama" name="nama" value="{{ $tipe->nama }}" placeholder="Ente Name">                         
                        </div>
                        
                        <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>

 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
@endsection