<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>SiReLo - Sistem Informasi Reservasi Lapangan Online</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('template/home/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{ asset('template/home/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/templatemo-digimedia-v1.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/owl.css') }}">
<!--

TemplateMo 568 DigiMedia

https://templatemo.com/tm-568-digimedia

-->
  </head>

<body>

  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <nav class="main-nav">
            <!-- ***** Logo Start ***** -->
            <a href="/" class="logo">
              <img src="{{ asset('template/home/images/logo4.svg') }}" alt="">
            </a>
            <!-- ***** Logo End ***** -->
            <!-- ***** Menu Start ***** -->
            <ul class="nav">
              <li class="scroll-to-section"><a href="#top" class="active">Beranda</a></li>
              <li class="scroll-to-section"><a href="#about">Tentang Kami</a></li>
              <li class="scroll-to-section"><a href="#contact">Contact</a></li> 
              <li class="scroll-to-section">
                <div class="border-first-button">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    {{-- <li class="nav-item ">
                     

                     
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                              @csrf
                          </form>
                    
                  </li> --}}
                  {{-- <button type="button" data-toggle="modal" data-target="#myModal">Login</button>
                  <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <!-- konten modal-->
                      <div class="modal-content">
                        <!-- heading modal -->
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Bagian heading modal</h4>
                        </div>
                        <!-- body modal -->
                        <div class="modal-body">
                          <p>bagian body modal.</p>
                        </div>
                        <!-- footer modal -->
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Tutup Modal</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Modal --> --}}

              </li> 
            </ul>        
            <a class='menu-trigger'>
                <span>Menu</span>
            </a>
            <!-- ***** Menu End ***** -->
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->

  <div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6 align-self-center">
              <div class="left-content show-up header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                <div class="row">
                  <div class="col-lg-12">
                    <h6>SiReLo</h6>
                    <h2>Sistem Informasi Reservasi Lapangan Online</h2>
                  </div>
                  <div class="col-lg-12">
                    <div class="border-first-button scroll-to-section">
                      <a href="/register">Daftar Sekarang</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="{{ asset('template/home/images/icon1.png') }}" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="about" class="about section">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-6">
              <div class="about-left-image  wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                <img src="{{ asset('template/home/images/orang.svg') }}" alt="">
              </div>
            </div>
            <div class="col-lg-6 align-self-center  wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
              <div class="about-right-content">
                <div class="section-heading">
                  <h6>Tentang Kami</h6>
                  <h4> <em>SiReLo</em></h4>
                  <div class="line-dec"></div>
                </div>
                <small>Dengan SIRELO, memberikan Pelayanan Penyewaan Lapangan Bulutangkis yang sesuai dengan keinginan anda.</small>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  <div id="portfolio" class="our-portfolio section">
    <div class="container">
      <div class="row">
        <div class="col-lg-5">
          <div class="section-heading wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
            <h6>Dokumentasi</h6>
            <h4>Foto Olahraga <em>SiReLo</em></h4>
            <div class="line-dec"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
      <div class="row">
        <div class="col-lg-12">
          <div class="loop owl-carousel">
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="{{ asset('template/home/images/k1.jpeg') }}" alt="">
                </div>
                <div class="down-content">
                  <h4>Sirelo</h4>
                </div>
              </div>
              </a>  
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="{{ asset('template/home/images/k2.jpeg') }}" alt="">
                </div>
                <div class="down-content">
                  <h4>Sirelo</h4>
                </div>
              </div>
              </a>  
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="{{ asset('template/home/images/k3.jpeg') }}" alt="">
                </div>
                <div class="down-content">
                  <h4>Sirelo</h4>
                </div>
              </div>
              </a>  
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="{{ asset('template/home/images/lapangan 1.jpeg') }}" alt="">
                </div>
                <div class="down-content">
                  <h4>Lapangn Sirelo</h4>
                </div>
              </div>
              </a>  
            </div>
            <div class="item">
              <a href="#">
                <div class="portfolio-item">
                <div class="thumb">
                  <img src="{{ asset('template/home/images/lapangan3.jpeg') }}" alt="">
                </div>
                <div class="down-content">
                  <h4>Lapangan Sirelo</h4>
                </div>
              </div>
              </a>  
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  

  <div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
            <h6>Hubungi Kami</h6>
            <h4>Info Lebih lanjut</h4>
            <div class="line-dec"></div>
          </div>
        </div>
        <div class="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" action="" method="post">
            <div class="row">
              <div class="col-lg-12">
                <div class="contact-dec">
                  <img src="{{ asset('template/home/images/contact-dec.png') }}" alt="">
                </div>
              </div>
              <div class="col-lg-5">
                <div id="map">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-4">
                        <div class="info-post">
                        <img src="{{ asset('template/home/images/logo4.svg') }}" alt="" align="center">
                          <h5>Sistem Reservasi Lapangan Online</h5>
                          </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="info-post">
                        <div class="icon">
                          <img src="{{ asset('template/home/images/phone-icon.png') }}" alt="">
                          <a href="#">+62 85326234672</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="info-post">
                        <div class="icon">
                          <img src="{{ asset('template/home/images/location-icon.png') }}" alt="">
                          <a href="#">Jl.Pahlawan,Surabaya</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022 Final Project., Ltd. Sanbercode. 
        </p>
        </div>
      </div>
    </div> 
  </footer> 


  <!-- Scripts -->
  <script src="{{ asset('template/home/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('template/home/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('template/home/js/owl-carousel.js') }}"></script>
  <script src="{{ asset('template/home/js/animation.js') }}"></script>
  <script src="{{ asset('template/home/js/imagesloaded.js') }}"></script>
  <script src="{{ asset('template/home/js/custom.js') }}"></script>

</body>
</html>