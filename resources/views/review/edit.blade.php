@extends('layouts.master')
@section('content')
 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Beri review</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Beri Review
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                            <form action="/review/{{ $review->review_id }}" method="POST">
                            @csrf 
                            @method('PUT')
                            <div class="card-body">
                            <div class="form-group">
                            <label for="nama">Nilai</label>
                            <input type="number" class="form-control" id="nilai" name="nilai" value="{{$review->nilai}}">
                            <input type="hidden" name="review_id" value="{{ $review->review_id}}">
                            
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Komentar</label>
                                <textarea class="form-control" rows="3" id="komentar" name="komentar">{{$review->komentar}}</textarea>
                                </div>
                            <div class="card-footer">
                                <a href="/review" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
@endsection