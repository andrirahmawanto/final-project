@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Review</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Review
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                        <div class="card-body">
                            <br>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 5%">Nama</th>
                                    <th style="text-align: center; width: 5%">Lapangan</th>
                                    <th style="text-align: center; width: 10%">Nilai</th>
                                    <th style="text-align: center; width: 15%">Komentar</th>
                                    <th style="text-align: center; width: 5%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($order as $key => $review)
                            <tr>
                                <td style="text-align: center">{{ $key + 1 }}</td>
                                <td>{{ $review->username }}</td>
                                <td>{{ $review->nama_lapangan }}</td>
                                <td style="text-align: center">{{ $review->nilai}}</td>
                                <td style="text-align: center">{{ $review->komentar }}</td>                                
                                <td style="text-align: center">                                                                        
                                    <form action="/review/{{ $review->review_id }}" method="POST">
                                        @csrf
                                            @method('DELETE')
                                            <input type="submit" value="Hapus" class="btn btn-danger btn-sm" >
                                          
                                    </form>                                   
                                </td>
                                {{-- <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td> --}}
                            </tr>
                            @endforeach                     
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    

@endpush
