@extends('layouts.master')
@section('content')
 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Beri review</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Beri Review
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                            <form action="/review" method="POST">
                            @csrf 
                            <div class="card-body">
                            <div class="form-group">
                            <label for="nama">Nilai</label>
                            <input type="number" class="form-control" id="nilai" name="nilai" placeholder="Berikan Nilai 1-5" >
                            <input type="hidden" name="lapangan_id" value="{{ $lapangan}}">
                            <input type="hidden" name="user_id" value="{{ $user}}">
                            
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Komentar</label>
                                <textarea class="form-control" rows="3" id="komentar" name="komentar" placeholder="Berikan Komentar"></textarea>
                                </div>
                            <div class="card-footer">
                                <a href="/review" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
@endsection