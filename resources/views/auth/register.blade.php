@extends('landing.home_master')
@section('content')
<div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-6 offset-lg-3 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-12">
                     
                      <div class="section-heading wow fadeIn">
                          <h4>Halaman <em>Pendaftaran</em></h4>
                          <div class="line-dec"></div>
                          <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" placeholder="Username" autofocus>
                                    <input type="hidden" name="status" value="2">
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                  </fieldset> 
                                  <fieldset>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                  </fieldset> 

                                <fieldset>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </fieldset>
                                <fieldset>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                                  </fieldset> 
                      <fieldset>
                        <button type="submit" id="form-submit" class="main-button ">{{ __('Register') }}  </button> <br>
                        <small>Sudah Punya Akun? <em><a href="\login">Login</a></em></small>
                      </fieldset>
                    </div>                
                  </div>
                </div>
                  </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection