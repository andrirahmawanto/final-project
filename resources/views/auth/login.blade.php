@extends('landing.home_master')
@section('content')
<div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-6 offset-lg-3 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-12">
                     
                      <div class="section-heading wow fadeIn">
                          <h4>Halaman <em>Login</em></h4>
                          <div class="line-dec"></div>
                          <div class="row">
                            <fieldset>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address" autocomplete="on" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            </fieldset>                              
                      <fieldset>
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      </fieldset>
                      <fieldset>
                        <button type="submit" class="btn btn-primary">
                            {{ __('Login') }}
                        </button>

                        {{-- @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif <br> --}}
                        <small>Belum Punya Akun? <em><a href="\register">Register</a></em></small>
                      </fieldset>
                    </div>                
                  </div>
                </div>
                  </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection