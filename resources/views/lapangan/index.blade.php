@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Lapangan</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Lapangan
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            <a class="btn btn-primary" href="/lapangan/create" >Tambah Lapangan</a>           
                        <div class="card-body">
                            <br>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 10%">Nama</th>
                                    <th style="text-align: center; width: 15%">Deskripsi</th>
                                    <th style="text-align: center; width: 5%">Tipe</th>
                                    <th style="text-align: center; width: 5%">Harga</th>
                                    <th style="text-align: center; width: 5%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($lapangan as $key => $lapangan)
                            <tr>
                                <td style="text-align: center">{{ $key + 1 }}</td>
                                <td>{{ $lapangan->nama_lapangan }}</td>
                                <td>{{ $lapangan->deskripsi }}</td>
                                <td style="text-align: center">{{ $lapangan->nama }}</td>
                                <td style="text-align: center">Rp. {{ $lapangan->harga_lapangan }}</td>                                
                                <td style="text-align: center">                                                                        
                                    <form action="/lapangan/{{ $lapangan->lapangan_id }}" method="POST">
                                    <a href="/lapangan/{{ $lapangan->lapangan_id }}/edit" class="btn btn-warning btn-sm" > Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm" >
                                    </form>                                   
                                </td>
                            </tr>
                            @endforeach                     
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    

@endpush
