@extends('landing.home_master')
@section('content')
<div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-6 offset-lg-3 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" action="" method="post">
            <div class="row">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-12">
                     
                      <div class="section-heading wow fadeIn">
                          <h4>Halaman <em>Pendaftaran</em></h4>
                          <div class="line-dec"></div>
                          <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                  <input type="text" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" autocomplete="on" required>
                                </fieldset>
                            <fieldset>
                              <input type="text" name="username" id="username" placeholder="Username" autocomplete="on" required>
                            </fieldset>
                            </div>
            
                          <div class="col-md-6">
                      <fieldset>
                        <input type="email" name="email" id="email" placeholder="email" autocomplete="on" required>
                      </fieldset>
                      <fieldset>
                        <input type="number" name="no_hp" id="no_hp" placeholder="Nomor Handphone" required>
                      </fieldset>
                          </div>
                      <fieldset>
                        <input type="password" name="password" id="password" placeholder="password" autocomplete="on">
                      </fieldset>
                      <fieldset>
                        <button type="submit" id="form-submit" class="main-button ">Daftar  </button> <br>
                        <small>Sudah Punya Akun? <em><a href="\login">Login</a></em></small>
                      </fieldset>
                    </div>                
                  </div>
                </div>
                  </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection