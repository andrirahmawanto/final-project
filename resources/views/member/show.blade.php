@extends('layouts.master')
@section('content')

<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Profil Member</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Profil Member
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                            <form action="/member/profil/{{ $profil->user_id }}" method="POST">
                            @csrf 
                            @method('PUT')
                            <div class="card-body">
                            <div class="form-group">
                            <label for="exampleInputNama">nama</label>
                            <input type="text" class="form-control" name="nama" value="{{$profil->nama}}">
                            </div>

                            <div class="form-group">
                            <label for="exampleInputEmail">alamat</label>
                            <input type="text" class="form-control" name="alamat" value="{{$profil->alamat}}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputUmur">Bio</label>
                                <textarea id="elm1" class="form-control" name="bio" rows="3" placeholder="Enter ...">{{ $profil->bio }}</textarea>                                
                                </div> 
                            </div>
                           
                            <div class="card-footer">
                                <a href="/member" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary" id="sa-success">Submit</button>
                            </div>
                    </form>
 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection
@push('scripts')


<script>
    $(document).ready(function () {
   if($("#elm1").length > 0){
     tinymce.init({
       selector: "textarea#elm1",
       theme: "modern",
       height:300,
       plugins: [
       "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
       "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
       "save table contextmenu directionality emoticons template paste textcolor"
       ],
       toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
       style_formats: [
       {title: 'Bold text', inline: 'b'},
       {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
       {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
       {title: 'Example 1', inline: 'span', classes: 'example1'},
       {title: 'Example 2', inline: 'span', classes: 'example2'},
       {title: 'Table styles'},
       {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
       ]
     });
   }
 });
</script>  

@endpush