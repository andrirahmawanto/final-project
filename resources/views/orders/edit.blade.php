@extends('layouts.master')
@section('content')
 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Booking Lapangan</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Booking Lapangan
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                        <form action="/order/{{ $order->order_id }}" method="POST">
                            @csrf 
                            @method('PUT')
                            <div class="card-body">
                            <div class="form-group">
                                <span for="order_tgl">Tanggal</span>
                                <input type="date" class="form-control" id="order_tgl" name="order_tgl" value="{{ $order->order_tgl }}">
                            </div>
                            <div class="form-group">
                                <span for="id_waktu">Jam</span><br>
                                <select name="id_waktu" id="id_waktu" class="form-control">
                                    <option value="{{ $order->waktu_id }}">{{ $order->waktu_nama }}</option>
                                    @foreach ($waktu as $waktu_id => $waktu_nama)
                                        <option value="{{ $waktu_id }}">{{ $waktu_nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <span for="lapangan">Lapangan</span>
                                <select name="id_lapangan" id="id_lapangan" class="form-control">
                                    <option value="{{ $order->lapangan_id }}">{{ $order->nama_lapangan }}</option>
                                    @foreach ($lapangan as $lapangan_id => $nama_lapangan)
                                        <option value="{{ $lapangan_id }}">{{ $nama_lapangan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="card-footer">
                                <a href="/order" class="btn btn-danger">Kembali</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
@endsection