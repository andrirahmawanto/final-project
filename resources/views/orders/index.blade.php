@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Booking Lapangan</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Booking Lapangan
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!--<div class="card-header">-->
                        <!--    <a class="btn btn-primary" href="/order/create" >Booking Lapangan</a>           -->
                        <!--<div class="card-body">-->
                        <!--    <br>-->

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 5%">Nama Member</th>
                                    <th style="text-align: center; width: 10%">Lapangan</th>
                                    <th style="text-align: center; width: 5%">Tgl Booking</th>
                                    <th style="text-align: center; width: 5%">Jam</th>
                                    <th style="text-align: center; width: 5%">Status</th>
                                    <th style="text-align: center; width: 5%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($order as $key => $order)
                            <tr>
                                <td style="text-align: center">{{ $key + 1 }}</td>
                                <td>{{ $order->username }}</td>
                                <td>{{ $order->nama_lapangan }}</td>
                                <td style="text-align: center">{{ $order->order_tgl }}</td>
                                <td style="text-align: center">{{ $order->waktu_nama }}</td>
                                <td style="text-align: center">{{ $order->order_status }}</td>                                
                                <td style="text-align: center">                                                                        
                                    <form action="/order/{{ $order->order_id }}" method="POST">
                                    <span class="btn btn-warning btn-sm" data-toggle="modal" data-target="#ModalAktiv{{ $order->order_id }}"> Konfirmasi</span>   
                                    <!--<a href="/order/{{ $order->order_id }}/edit" class="btn btn-warning btn-sm" > Edit</a>-->
                                    <!--    @csrf-->
                                    <!--    @method('DELETE')-->
                                    <!--    <input type="submit" value="Hapus" class="btn btn-danger btn-sm" >-->
                                    </form>                                   
                                </td>
                            </tr>
                            <div class="modal fade" id="ModalAktiv{{ $order->order_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <form action="/order/konfirmasi/{{ $order->order_id }}" method="POST" enctype='multipart/form-data'>
                                                @csrf 
                                                @method('PUT')
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
                                                </div>
                                                <div class="modal-body">
                                                <div class="form-group">
                                                    <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan konfirmasi order dari : {{ $order->username }}  ?</label>
                                                    </div>  
                                                </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                            <button type="submit"  class="btn btn-success">Aktiv</button>
                                        </div>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            @endforeach                     
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    

@endpush
