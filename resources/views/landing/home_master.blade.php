
@include('landing.layouts.header')
<div class="wrapper">
@include('landing.layouts.navbar')
@yield('content')

@include('landing.layouts.footer')
@stack('scripts')
