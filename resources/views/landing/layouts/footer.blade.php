<footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022 Final Project., Ltd. Sanbercode. 
        </p>
        </div>
      </div>
    </div> 
  </footer> 


  <!-- Scripts -->
  <script src="{{ asset('template/home/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('template/home/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('template/home/js/owl-carousel.js') }}"></script>
  <script src="{{ asset('template/home/js/animation.js') }}"></script>
  <script src="{{ asset('template/home/js/imagesloaded.js') }}"></script>
  <script src="{{ asset('template/home/js/custom.js') }}"></script>

</body>
</html>