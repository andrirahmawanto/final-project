<body>

    <!-- ***** Preloader Start ***** -->
    <div id="js-preloader" class="js-preloader">
      <div class="preloader-inner">
        <span class="dot"></span>
        <div class="dots">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
    <!-- ***** Preloader End ***** -->
  
    <!-- ***** Header Area Start ***** -->
    <header class="header-area header-sticky wow slideInDown" data-wow-duration="0.75s" data-wow-delay="0s">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <nav class="main-nav">
              <!-- ***** Logo Start ***** -->
              <a href="\" class="logo">
                <img src="{{ asset('template/home/images/logo4.svg') }}" alt="">
              </a>
              <!-- ***** Logo End ***** -->
              <!-- ***** Menu Start ***** -->
              <ul class="nav">
                <li class="scroll-to-section"><a href="\" class="active">Beranda</a></li>
                <li class="scroll-to-section"><a href="#about">Tentang Kami</a></li>
                <!-- <li class="scroll-to-section"><div class="border-second-button"><a href="#portfolio">Register</a></div></li> -->
                <!-- <li class="scroll-to-section"><a href="#blog">Blog</a></li> -->
                <li class="scroll-to-section"><a href="#contact">Contact</a></li> 
                <li class="scroll-to-section"><div class="border-first-button"><a href="/login">Login</a></div></li> 
              </ul>        
              <a class='menu-trigger'>
                  <span>Menu</span>
              </a>
              <!-- ***** Menu End ***** -->
            </nav>
          </div>
        </div>
      </div>
    </header>