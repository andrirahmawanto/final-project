  <head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
     <!-- CSRF Token -->
     <meta name="csrf-token" content="{{ csrf_token() }}">         
 
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>SiReLo - Sistem Informasi Reservasi Lapangan Online</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('template/home/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{ asset('template/home/css/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/templatemo-digimedia-v1.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/animated.css') }}">
    <link rel="stylesheet" href="{{ asset('template/home/css/owl.css') }}">
    
  </head>