<!DOCTYPE html>
<html lang="en">
@include('layouts.partials.header')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
@include('layouts.partials.navbar')
@include('layouts.partials.sidebar')
@yield('content')
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

@include('layouts.partials.footer')
@stack('scripts')
</body>
</html>
