<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="sirelo">
    <meta name="keywords" content="sirelo">
    <meta name="author" content="sirelo">
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('template/images/logo/icon.png') }}">
    <!-- App title -->
    <title>SIRELO - Sistem Informasi Reservasi Lapangan Online</title>
    <!-- Jquery Ui -->
    <link href="{{ asset('template/plugins/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet" type="text/css" />
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ asset('template/plugins/morris/morris.css') }}">
    
    <!--Form Wizard-->
    <link rel="stylesheet" type="text/css" href="{{ asset('template/plugins/jquery.steps/css/jquery.steps.css"') }} />
    <link href="{{ asset('template/plugins/summernote/summernote.css" rel="stylesheet') }}" />
    <!-- App css -->
    <link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/core.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/components.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/icons.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/pages.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/menu.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('template/css/responsive.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('template/plugins/switchery/switchery.min.css') }}">
    <!-- DataTables -->
    <link href="{{ asset('template/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('template/plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>


    {{-- <link rel="stylesheet" href="{{ asset('template/plugins/switchery/switchery.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">   
    <link type="text/css" href="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css" rel="stylesheet" />      --}}
    <script src="{{ asset('template/js/modernizr.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.min.js') }}"></script>
    <script src="{{ asset('template/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ asset('template/plugins/raphael/raphael-min.js') }}"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>

</head>
