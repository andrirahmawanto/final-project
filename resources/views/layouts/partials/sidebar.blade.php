<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="padding-top: 8%">
	<div class="sidebar-inner slimscrollleft">
		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<ul>
					<li>
						<a href="{{ url('/dashboard') }}" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
					</li>
                    <li>
						<a href="{{ url('/order') }}" class="waves-effect"><i class="mdi mdi-receipt"></i> <span> Data Order </span></a>
					</li>
                    <li>
						<a href="{{ url('/review') }}" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i> <span> Data Review </span></a>
					</li>
                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-settings"></i><span> Data Master </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ url('/tipe') }}">Tipe Lapangan</a></li>
                            <li><a href="{{ url('/lapangan') }}">Lapangan</a></li>
                            <li><a href="{{ url('/member') }}">Member</a></li>
                            <li><a href="{{ url('/pegawai') }}">Pegawai</a></li>
                        </ul>
                    </li>
                    <li>
						<a href="{{ url('/profil') }}" class="waves-effect"><i class="mdi mdi-account-card-details"></i> <span> Profil </span></a>
					</li>
                    <li>
						<a href="{{ url('/reservasi') }}" class="waves-effect"><i class="mdi mdi-receipt"></i> <span> Reservasi </span></a>
					</li>
                    <li>
						<a href="{{ url('/review_member') }}" class="waves-effect"><i class="mdi mdi-comment-text-outline"></i> <span> Review </span></a>
					</li>
                </ul>
</div>
<!-- Sidebar -->
<div class="clearfix"></div>
</div>
<!-- Sidebar -left -->
</div>
<!-- Left Sidebar End