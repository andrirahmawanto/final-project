<body class="fixed-left">
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Top Bar Start -->
        <div class="topbar">
            <!-- LOGO -->
            <div class="topbar-left">
                <a class="logo"><span style="font-size:32px; font-weight: bold;">SIRELO</span><i></i></a>
                <!-- Image logo -->
                <a class="logo">
                    <span>
                        <img src="{{ asset('template/images/logo/logo-sidebar.png') }}" alt="" height="90">
                    </span>
                    <i>
                        <img src="{{ asset('template/images/logo/logo-sidebar-sm.png') }}" alt="" height="40">
                    </i>
                </a>
            </div>
            <!-- Button mobile view to collapse sidebar menu -->
            <div class="navbar navbar-default" role="navigation">
                <div class="container">
                    <!-- Navbar-left -->
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <button class="button-menu-mobile open-left waves-effect">
                                <i class="mdi mdi-tune"></i>
                            </button>
                        </li>
                    </ul>
                    <!-- Right(Notification) -->
                    <ul class="nav navbar-nav navbar-right">
                        
                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{ asset('template/images/users/user-2.png') }}" alt="user-img" class="img-circle user-img">
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li>
                                <h5>{{ Auth::user()->username }}</h5>
                            </li>
                                    <li align="center"> <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                      document.getElementById('logout-form').submit();">
                                         {{ __('Logout') }}
                                     </a>
           
                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                     </form></li>
                                </ul>
                            </li>
                        </ul> <!-- end navbar-right -->
                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->