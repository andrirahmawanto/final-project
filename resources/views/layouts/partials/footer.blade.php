<footer class="footer text-right">
  <strong> &copy; 2022 FINAL PROJECT SANBERCODE </strong>
</footer>
<script>
  var resizefunc = [];
</script>
<!-- jQuery  -->
<script src="{{ asset('template/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('template/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('template/js/detect.js') }}"></script>
<script src="{{ asset('template/js/fastclick.js') }}"></script>
<script src="{{ asset('template/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('template/js/waves.js') }}"></script>
<script src="{{ asset('template/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('template/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('template/plugins/switchery/switchery.min.js') }}"></script>
<!-- Datatable  -->
<script src="{{ asset('template/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('template/plugins/datatables/dataTables.colVis.js') }}"></script>
 <script src="{{ asset('template/plugins/datatables/dataTables.fixedColumns.min.js') }}"></script> 

<!-- init -->
<script src="{{ asset('template/pages/jquery.datatables.init.js') }}"></script>

<!-- Counter js  -->
<script src="{{ asset('template/plugins/waypoints/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('template/plugins/counterup/jquery.counterup.min.js') }}"></script>

<!--Morris Chart-->
<script src="{{ asset('template/plugins/morris/morris.min.js') }}"></script>
<script src="{{ asset('template/plugins/raphael/raphael-min.js') }}"></script>
<script src="{{ asset('template/pages/jquery.morris.init.js') }}"></script>

<!-- Dashboard init -->
<script src="{{ asset('template/pages/jquery.dashboard.js') }}"></script>

<!--Summernote js-->
<script src="{{ asset('template/plugins/summernote/summernote.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('template/js/jquery.core.js') }}"></script>
<script src="{{ asset('template/js/jquery.app.js') }}"></script>

<!-- Modal-Effect -->
<script src="{{ asset('template/plugins/custombox/js/custombox.min.js') }}"></script>
<script src="{{ asset('template/plugins/custombox/js/legacy.min.js') }}"></script>

<!--Form Wizard-->
<script src="{{ asset('template/plugins/jquery.steps/js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('template/plugins/jquery-validation/js/jquery.validate.min.js') }}"></script>
<!--wizard initialization-->
<script src="{{ asset('template/pages/jquery.wizard-init.js') }}"></script>

<!-- Jquery ui js -->
<script src="{{ asset('template/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('template/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>

<!-- Datatable Kelas -->
<script>
  $(function () {
                // sortable
                $(".sortable").sortable({
                  connectWith: '.sortable',
                  items: '.card-draggable',
                  revert: true,
                  placeholder: 'card-draggable-placeholder',
                  forcePlaceholderSize: true,
                  opacity: 0.77,
                  cursor: 'move'
                });
              });
TableManageButtons.init();
</script>
<!-- https://jsfiddle.net/gyrocode/snqw56dw/ -->
<!---->