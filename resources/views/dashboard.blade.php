@extends('layouts.master')
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Dashboard</h4>
                        <ol class="breadcrumb p-0 m-0">
                            <!-- <li>
                                <a href="#"></a>
                            </li> -->
                           <!--  <li>
                                <a href="#">Dashboard</a>
                            </li> -->
                            <li class="active">
                                Dashboard
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                @foreach ($lapangan as $key => $lapangan)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-view-module widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total Lapangan</p>
                            <h2>{{ $lapangan->jumlah }}</h2>
                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
                @foreach ($pegawai as $key => $pegawai)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-account-convert widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User Today">Total Pegawai</p>
                            <h2>{{ $pegawai->jumlah }}</h2>
                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
                @foreach ($tipe as $key => $tipe)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-layers widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">Jenis Lapangan</p>
                            <h2>{{ $tipe->jumlah }}</h2>
                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
                @foreach ($member as $key => $member)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-account-multiple widget-one-icon"></i>                        
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Request Per Minute">Total Member</p>
                            <h2>{{ $member->jumlah }}</h2>                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
                @foreach ($order as $key => $order)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-av-timer widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Total Users">Total Order</p>
                            <h2>{{ $order->jumlah }}</h2>                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
                @foreach ($review as $key => $review)
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card-box widget-box-one">
                        <i class="mdi mdi-message-text-outline widget-one-icon"></i>
                        <div class="wigdet-one-content">
                            <p class="m-0 text-uppercase font-600 font-secondary text-overflow">Total Review</p>
                            <h2>{{ $review->jumlah }}</h2>
                            
                        </div>
                    </div>
                </div><!-- end col -->
                @endforeach
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->          
</div>
</div>
</div>
        <!-- END wrapper -->
@endsection

@push('scripts')      
<script>
    $(document).ready(function() {    
      swal("Selamat Datang", "", "success");
    });
    </script>
@endpush