@extends('landing.home_master')
@section('content')
<div id="contact" class="contact-us section">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-6 offset-lg-3 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
          <form id="contact" action="" method="post">
            <div class="row">
                <div class="fill-form">
                  <div class="row">
                    <div class="col-lg-12">
                     
                      <div class="section-heading wow fadeIn col-lg-8 offset-lg-2">
                          <h4>Halaman <em>Login</em></h4>
                          <div class="line-dec"></div>
                      
                      <fieldset>
                        <input type="text" name="username" id="name" placeholder="Name" autocomplete="on" required>
                      </fieldset>
                      <fieldset>
                        <input type="password" name="password" id="password" placeholder="password" autocomplete="on">
                      </fieldset>
                      <fieldset>
                        <button type="submit" id="form-submit" class="main-button ">Masuk</button> <br>
                        <small>Belum Punya Akun? <em><a href="/register">Daftar</a></em></small>
                      </fieldset>
                    </div>                
                  </div>
                </div>
                  </div>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection