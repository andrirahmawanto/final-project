@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Pegawai</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Pegawai
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-header">
                            <a class="btn btn-primary" href="/pegawai/create" >Tambah Pegawai</a>           
                        <div class="card-body">
                            <br>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 10%">Nama Pegawai</th>
                                    <th style="text-align: center; width: 10%">Email</th>
                                    <th style="text-align: center; width: 10%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($users as $key => $users)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $users->username }}</td>
                                <td>{{ $users->email }}</td>                                
                                <td style="display: flex">                                                                        
                                    <form action="/pegawai/{{ $users->id }}" method="POST">
                                     @if( $users->status == 0)
                                    <span class="btn btn-info btn-sm" data-toggle="modal" data-target="#ModalAktiv{{ $users->id }}"><i class="fa fa-users"></i> | Aktivasi</span>
                                   @else
                                   <a href="/pegawai/{{ $users->id }}" class="btn btn-primary btn-sm" > Profil</a>
                                    <a href="/pegawai/{{ $users->id }}/edit" class="btn btn-warning btn-sm" > Edit</a>
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Hapus" class="btn btn-danger btn-sm" >
                                    @endif
                                    </form>                                   
                                </td>
                            </tr>
                                        <div class="modal fade" id="ModalAktiv{{ $users->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <form action="/pegawai/aktivasi/{{ $users->id }}" method="POST" enctype='multipart/form-data'>
                                                @csrf 
                                                @method('PUT')
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Aktivasi</h4>
                                                </div>
                                                <div class="modal-body">
                                                <div class="form-group">
                                                    <input type="hidden" id="id" name="id" value="{{ $users->id }}">
                                                    <input type="hidden"  name="username" value="{{ $users->username }}">
                                                    <input type="hidden"  name="email" value="{{ $users->email }}">
                                                    <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan aktivasi : {{ $users->username }}  ?</label>
                                                    </div>  
                                                </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                            <button type="submit"  class="btn btn-danger">Aktiv</button>
                                        </div>
                                        </form>
                                    </div>
                                    </div>
                                </div>
                            @endforeach   
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    
<script>
    $(document).ready(function() {    
      swal("Selamat Datang di Halaman Admin", "", "success");
    });
    </script>
@endpush
