@extends('layouts.master')
@section('content')
 <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Pegawai</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            Data Pegawai
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive"> 
                            <form action="/pegawai" method="POST">
                            @csrf 
                            <div class="card-body">
                            <div class="form-group">
                            <label for="exampleInputNama">Username</label>
                            <input type="text" class="form-control" name="username" placeholder="Masukan Tipe Username">
                            </div>

                            <div class="form-group">
                            <label for="exampleInputEmail">Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Masukan Email">
                            </div>

                            <div class="form-group">
                            <label for="exampleInputPassword">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Masukan Password">
                            </div>

                            <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
 
                    </div>
                </div>
            </div>
        </div> <!-- container -->

    </div> <!-- content -->

</div>

</div>
<!-- END wrapper -->
@endsection