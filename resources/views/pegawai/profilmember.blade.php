@extends('layouts.master')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="page-title-box">
                                    <h4 class="page-title">Data Profil</h4>
                                    <ol class="breadcrumb p-0 m-0">
                                        <li>
                                            <a href="#">Dashboard </a>
                                        </li>
                                        <li class="active">
                                            data profil
                                        </li>
                                    </ol>
                                    <div class="clearfix"></div>
                                </div>
                            </div>                      
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <table id="example1" class="table table-striped table-bordered dt-responsive nowrap">
                            <thead>
                                <tr>
                                    <th style="text-align: center; width: 1%">No</th>
                                    <th style="text-align: center; width: 10%">Nama</th>
                                    <th style="text-align: center; width: 10%">Email</th>
                                    <th style="text-align: center; width: 10%">Alamat</th>
                                    <th style="text-align: center; width: 10%">Bio</th>
                                    <th style="text-align: center; width: 10%">Fitur</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @foreach ($users as $key => $users)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $users->nama }}</td>
                                <td>{{ $users->email }}</td>
                                <td>{{ $users->alamat }}</td>                                
                                <td>{{ $users->bio }}</td>
                                <td style="display: flex">                                                                        
                                    <form action="/pegawai/{{ $users->id }}" method="POST">                                   
                                   <a href="/pegawai/{{ $users->id }}" class="btn btn-primary btn-sm" > Edit</a>                                    
                                    </form>                                   
                                </td>
                            </tr>                                       
                            @endforeach   
                            </tbody>
            </table>
</div>
</div>
</div>
</div> <!-- container -->

</div> <!-- content -->

</div>

</div>
<!-- END wrapper -->

@endsection

@push('scripts')
<script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    
@endpush
