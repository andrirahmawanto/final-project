<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
Route::get('/order', function () {
    return view('orders.index');
});
Route::get('/review', function () {
    return view('review.index');
});
Route::get('/login', function(){
    return view('login.login');
});
Route::get('/register', function(){
    return view('register.register');
});
Route::resource('tipe', 'TipeController');
Route::resource('lapangan', 'LapanganController');
Route::resource('pegawai', 'PegawaiController');
Route::put('/pegawai/aktivasi/{id}', 'PegawaiController@update2');
Route::put('/pegawai/profil/{id}', 'PegawaiController@updateprofil');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/profil', 'PegawaiController@profil');

Route::resource('member', 'MemberController');
Route::put('/member/profil/{id}', 'MemberController@updateprofil');
Route::put('/member/aktivasi/{id}', 'MemberController@update2');

Route::resource('order', 'OrderController');
Route::put('/order/konfirmasi/{order_id}', 'OrderController@konfirmasi');
Route::get('/reservasi', 'OrderController@reservasi');

Route::get('/review_member', 'ReviewController@review_member');
Route::resource('review', 'ReviewController');
Route::put('/review/{id}', 'ReviewController@update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
