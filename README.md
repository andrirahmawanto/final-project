# Final Proejct
# Kelompok 13
# Anggota Kelompok
<li>Muhammad Andri Rahmawanto</li><br>
<li>Mujibur Rohman</li><br>
<li>Roy Achmad Aziz</li>


# Tema Project
<p> Aplikasi Peminjaman Lapangan Bulutangkis</p>

# ERD
![ERD_Final_project.png](ERD_Final_project.png?raw=true)

# Link Video
<p> Link Demo Aplikasi : <a href=" https://drive.google.com/drive/folders/1VxJrnfFVBDoQU1WI2AY1LWRppzG1IcOb?usp=sharing">https://drive.google.com/drive/folders/1VxJrnfFVBDoQU1WI2AY1LWRppzG1IcOb?usp=sharing</a></p>
<p>Link Deploy  :  <a href="https://laravel.sidigital.co.id/">https://laravel.sidigital.co.id</a></p>
